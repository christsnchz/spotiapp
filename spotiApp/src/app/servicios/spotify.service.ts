import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  Token:string ;

  ActualizarToken(token:string){
    this.Token = token;
  }

  getQuery(query: string) {

    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.Token}`
    });

    return this.http.get(url, { headers });
  }

  getNuevosLanzamientos() {

    return this.getQuery('browse/new-releases?limit=21')
      .pipe(map(data => (
        data['albums'].items
      )));
  }

  getArtistas(termino: string) {

    return this.getQuery(`search?q=${termino}&type=artist&limit=21`)
      .pipe(map(data => (
        data['artists'].items
      )));
  }

  getArtista(id: string) {
    return this.getQuery(`artists/${id}`);
  }

  getTopTracks(id : string){
    return this.getQuery(`artists/${id}/top-tracks?country=us`)
    .pipe( map( data => (
        data['tracks']
    )));
  }

}
