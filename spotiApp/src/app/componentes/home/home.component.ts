import { Component } from '@angular/core';
import { SpotifyService } from 'src/app/servicios/spotify.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {

  nuevasCanciones: any[] = [];
  loading : boolean;
  error = false;
  mensajeError:string;

  

  constructor( private spotify: SpotifyService,private router:Router) {

    this.loading = true;
    this.spotify.getNuevosLanzamientos().subscribe( (data :any) => {
      this.nuevasCanciones = data;
      this.loading = false;
    }, (errorServicio) => {

      this.error = true
      this.loading = false;
      this.mensajeError = errorServicio.error.error.message;
    } );
   }

   ActualizarToken(token:string){
      this.spotify.ActualizarToken(token);
      this.router.navigate(['/']);
      this.spotify.getNuevosLanzamientos().subscribe( (data :any) => {
        this.nuevasCanciones = data;
        this.loading = false;
        this.error = false;
      });
   }


}
